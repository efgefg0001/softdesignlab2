package org.iu761.vns.rawtcpport;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.*;
import java.io.IOException;

import org.apache.commons.lang3.SerializationUtils;
import org.iu761.vns.ClientData;
import org.iu761.vns.ServerPort;
import org.iu761.vns.utils.Message;
import org.iu761.vns.exceptions.*;
import org.iu761.vns.logger.*;
import org.iu761.vns.crypto.base.Key;

import java.util.concurrent.ConcurrentMap;

public class RawTcpServerPort extends ServerPort {

    private String nameOfNewClient;
    private Key publicKeyOfNewClient;

    public RawTcpServerPort(int portNumber, ConcurrentMap<String, ClientData> clientMesInfo) {
        super(portNumber, clientMesInfo);
    }

    private boolean connectToServer(Socket socket) {
        boolean result = false;
        ObjectInputStream input = null;
        ObjectOutputStream output = null;
        try{
            input = new ObjectInputStream(socket.getInputStream());
            output = new ObjectOutputStream(socket.getOutputStream());
            Message request = (Message)input.readObject();
            publicKeyOfNewClient = SerializationUtils.deserialize(request.getContent());
            nameOfNewClient = request.getSender();
            Message response;
            if(getClientMesInfo().containsKey(nameOfNewClient)) {
                response = new Message(
                        "", request.getSender(),
                        new byte[0],
                        "response",
                        new ClientReconnectionException()
                );
            } else {
                result = true;
                response = new Message(
                        "", request.getSender(),
                         "OK".getBytes(),
                        "response"
                );
            }
            output.writeObject(response);
        } catch (Exception error) {
            System.err.println(error.getMessage());
        } finally {
            if(!result)
                try {
                    socket.close();
                } catch (Exception error) {
                    System.err.println(error.getMessage());
                }
        }
        return result;
    }

    @Override
    public void run() {
        super.run();
        try (ServerSocket serverSocket = new ServerSocket(getPortNumber())) {
            while (isListening()) {
                Socket socket = serverSocket.accept();
                System.out.println("Incoming connection from " + socket.getInetAddress());
                if(connectToServer(socket)) {
                    getClientMesInfo().put(nameOfNewClient, new ClientData(publicKeyOfNewClient));
                    getLogger().log(LogMessage.CLIENT_CONNECT, "Client \"" + nameOfNewClient + "\"connected to server.");
                    getExecClients().execute(new RawTcpServerClient(nameOfNewClient, getClientMesInfo(), socket, getLogger()));
                }
            }
        } catch (IOException error) {
            System.err.println("Could not listen on port " + getPortNumber());
        } catch (Exception error) {
            System.err.println(error.getMessage());
        }
    }
}
