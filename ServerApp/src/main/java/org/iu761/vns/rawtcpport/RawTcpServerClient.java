package org.iu761.vns.rawtcpport;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.concurrent.ConcurrentMap;
import java.util.function.*;
import java.util.*;

import org.apache.commons.lang3.SerializationUtils;
import org.iu761.vns.ClientData;
import org.iu761.vns.ServerClient;
import org.iu761.vns.crypto.base.Key;
import org.iu761.vns.exceptions.*;
import org.iu761.vns.logger.LogMessage;
import org.iu761.vns.logger.Logger;
import org.iu761.vns.utils.*;

class RawTcpServerClient extends ServerClient {

    private ConcurrentMap<String, ClientData> clientMesInfo;
    private String name;
    private Socket socket;
    private Map<String, Function<Message, Message>> commands;
    private Logger logger;

    public RawTcpServerClient(String name, ConcurrentMap<String, ClientData> clientMesInfo, Socket socket, Logger logger) {
        this.name = name;
        this.clientMesInfo = clientMesInfo;
        this.socket = socket;
        this.commands = createCommands();
        this.logger = logger;
    }

    private Map<String, Function<Message, Message>> createCommands() {
        Map<String, Function<Message, Message>> commands = new HashMap<>();
// clients command
        commands.put("clients", request -> {
            HashMap<String, Key> clientsWithPublKeys= new HashMap<>();
            synchronized (clientMesInfo) {
                for(String client : clientMesInfo.keySet())
                    clientsWithPublKeys.put(client, clientMesInfo.get(client).getPublicKey());
            }
            return new Message(
                    "server", name,
                    SerializationUtils.serialize(clientsWithPublKeys),
                    "clients");
        });

// downloadable command
        commands.put("downloadable", request -> {
            Message response = new Message(name, "server", "ERROR".getBytes(), "downloadable");
            if (clientMesInfo.containsKey(name)) {
                ArrayList<String> downloadable = new ArrayList<>();
                List<Message> downloads = clientMesInfo.get(name).getDownloadable();
                synchronized (downloads) {
                    for (Message mes : downloads)
                        downloadable.add(mes.getName());
                }
                response = new Message(
                        name, "server",
                        SerializationUtils.serialize(downloadable),
                        "downloadable"
                );

            } else {
                response = new Message(name, "server",
                        new byte[0],
                        "downloadable",
                        new NoClientWithSuchNameOnServerException(request.getSender())
                );
            }
            return response;
        });

// download command
        commands.put("download", request -> {
            Message response = null;
            if(!clientMesInfo.containsKey(request.getSender()))
                response = new Message(
                        "server", request.getSender(),
                        new byte[0],
                        "download",
                        new NoClientWithSuchNameOnServerException(request.getSender())
                );
            else {
                String mesName = new String(request.getContent());
                Optional<Message> item = clientMesInfo.get(request.getSender()).getDownloadable()
                    .stream()
                    .filter(message-> message.getName().equals(mesName))
                    .findFirst();
                if(item == null)
                    response = new Message("server", request.getSender(),
                            new byte[0],
                            "download",
                            new NoSuchElementException(request.getName())
                    );
                else {
                    response = item.get();
                    clientMesInfo.get(request.getSender()).getDownloadable().remove(response);
                }
            }
            return response;
        });

// disconnect command
        commands.put("disconnect", request -> {
            Message response = null;
            if(!clientMesInfo.containsKey(request.getSender()))
                response = new Message(
                        "server", request.getSender(),
                        new byte[0],
                        "disconnect",
                        new NoClientWithSuchNameOnServerException(request.getSender())
                );
            else {
                clientMesInfo.get(request.getSender()).stopListen();
                response = new Message(
                        "server", request.getSender(),
                        "OK".getBytes(),
                        "disconnect"
                );
            }
            return response;
        });

// upload command
        commands.put("upload", request -> {
            Message response = null;
            String receiver = request.getReceiver();
            if(clientMesInfo.containsKey(receiver)) {
                clientMesInfo.get(receiver).getDownloadable().add(request);
                response = new Message("server", name, "OK".getBytes(), request.getName());
            } else
                response = new Message(
                        "server", name,
                        new byte[0],
                        request.getName(),
                        new NoClientWithSuchNameOnServerException(receiver)
                );
            return response;
        });

        return commands;
    }

    private Message analyze(Message request) {
        String command = request.getName().trim().toLowerCase();
        if(!commands.keySet().contains(command))
            command = "upload";
        return commands.get(command).apply(request);
    }
    @Override
    public void run() {
        try {
            ClientData clientData = clientMesInfo.get(name);
            while (clientData.shouldListen()) {
                ObjectInputStream input = new ObjectInputStream(socket.getInputStream());
                ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
                logger.log(LogMessage.SEND_INFO, "Got message from client \"" + name + "\"");
                Message response = analyze((Message) input.readObject());
                output.writeObject(response);
                logger.log(LogMessage.SEND_INFO, "Sent response to client \"" + name + "\"");
            }
        } catch (Exception error) {
            try {
                logger.log(LogMessage.SEND_ERROR, error.getMessage());
            } catch (Exception ex) {}
//            System.err.println(error.getMessage());
        } finally {
            try {
                socket.close();
            } catch (Exception error) {
                System.err.println(error.getMessage());
            }
            clientMesInfo.remove(name);
            try {
                logger.log(LogMessage.CLIENT_DISCONNECT, "Client \"" + this.name +"\" disconnected.");
            } catch (Exception error) {}
        }
    }
}
