package org.iu761.vns;

import java.util.*;
import java.util.concurrent.*;

import org.iu761.vns.utils.Message;
import org.iu761.vns.crypto.base.Key;

public class ClientData {

    private List<Message> dowloadable = Collections.synchronizedList(new ArrayList<>());
    private boolean shouldListen = true;
    private Key publicKey = null;

    public ClientData(Key publicKey) {
        this.publicKey = publicKey;
    }

    public Key getPublicKey() {
        return publicKey;
    }

    public List<Message> getDownloadable() {
        return dowloadable;
    }

    public boolean shouldListen() {
        return shouldListen;
    }

    public synchronized void stopListen() {
        shouldListen = false;
    }
}
