package org.iu761.vns;

import org.iu761.vns.logger.LogMessage;
import org.iu761.vns.logger.Logger;
import org.iu761.vns.logger.Type;
import sun.rmi.runtime.Log;

import java.util.concurrent.*;
import java.util.*;

public class Server {

    private ExecutorService execPorts;
    private boolean listening;
    private Map<Integer, ServerPort> ports;
    private ConcurrentMap<String, ClientData> clientMesInfo = new ConcurrentHashMap<>();
    private Logger logger = Logger.getInstance(Type.SERVER);

    public Server(Map<Integer, ServerPort> ports) {
        this.ports = ports;
        execPorts = Executors.newFixedThreadPool(ports.size());
        for(ServerPort port : this.ports.values())
            port.setLogger(logger);
    }
    public void loop() throws Exception {
        logger.log(LogMessage.START, "Server started.");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                execPorts.shutdownNow();
                try {
                    logger.log(LogMessage.STOP, "Server stopped.");
                } catch(Exception error) {}
            }
        });
        for(int nPort : ports.keySet())
            execPorts.execute(ports.get(nPort));
        execPorts.shutdown();
        /*
        try {
            execPorts.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException exception) {
        } finally {
            logger.log(LogMessage.STOP, "Server stopped.");
        }*/

   }

}
