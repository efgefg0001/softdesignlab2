package org.iu761.vns;

import java.util.concurrent.*;
import java.io.*;
import java.util.*;

import org.iu761.vns.logger.*;

public abstract class ServerPort implements Runnable {

    private Logger logger = null;
    private int portNumber;
    private ConcurrentMap<String, ClientData> clientMesInfo;
    private ExecutorService execClients = Executors.newCachedThreadPool();
    private boolean listening = true;

    public ServerPort(int portNumber, ConcurrentMap<String, ClientData> clientMesInfo) {
        this.portNumber = portNumber;
        this.clientMesInfo = clientMesInfo;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    public Logger getLogger() {
        return logger;
    }

    public boolean isListening() {
        return listening;
    }

    public void stopListening() {
        listening = false;
    }

    public ExecutorService getExecClients() {
        return execClients;
    }

    public int getPortNumber() {
        return portNumber;
    }

    public ConcurrentMap<String, ClientData> getClientMesInfo() {
        return clientMesInfo;
    }

    @Override
    public void run() {
        System.out.println("Listening on port " + getPortNumber());

    }
}


