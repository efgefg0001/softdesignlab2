package org.iu761.vns.httpport;

import org.apache.http.*;
import org.apache.http.protocol.*;
import org.iu761.vns.ClientData;

import java.io.IOException;
import java.util.concurrent.ConcurrentMap;

class DisconnectReqHandler implements HttpRequestHandler {

    private ConcurrentMap<String, ClientData> clientMesInfo;

    public DisconnectReqHandler(ConcurrentMap<String, ClientData> clientMesInfo) {
        this.clientMesInfo = clientMesInfo;
    }

    @Override
    public void handle(HttpRequest request, HttpResponse httpResponse, HttpContext httpContext) throws HttpException, IOException {
        Header header = request.getFirstHeader("name");
        String name = header.getValue();
        clientMesInfo.get(name).stopListen();
        httpResponse.setStatusCode(HttpStatus.SC_OK);
    }
}
