package org.iu761.vns.httpport;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.http.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.*;
import org.apache.http.util.*;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;

import org.iu761.vns.*;
import org.iu761.vns.utils.Message;
import org.iu761.vns.crypto.base.Key;

class ConnectToServerReqHandler implements HttpRequestHandler {
    private ConcurrentMap<String, ClientData> clients;
    private Connection connection;
    private StringBuilder outerRefName;

    public ConnectToServerReqHandler(StringBuilder outerRefName,
                                     ConcurrentMap<String, ClientData> clients,
                                     Connection connection) {
        this.clients = clients;
        this.outerRefName = outerRefName;
        this.connection = connection;
    }

    @Override
    public void handle(HttpRequest request, HttpResponse httpResponse, HttpContext httpContext)
            throws HttpException, IOException {
        System.out.println("ConnectToServerReqHandler");
        if (request instanceof HttpEntityEnclosingRequest) {
            HttpEntity entity = ((HttpEntityEnclosingRequest) request).getEntity();
            Message message = SerializationUtils.deserialize(EntityUtils.toByteArray(entity));
            String name = message.getSender();
            if (clients.containsKey(name)) {
                httpResponse.setStatusCode(HttpStatus.SC_METHOD_FAILURE);
                String resp = "There is client with such name connected to the server.";
                httpResponse.setEntity(new StringEntity(resp));
                throw new HttpException(resp);
            } else {
                outerRefName.append(name);
                httpResponse.setStatusCode(HttpStatus.SC_OK);
                Key publicKey = SerializationUtils.deserialize(message.getContent());
                clients.put(name, new ClientData(publicKey));
            }
        }
    }
}
