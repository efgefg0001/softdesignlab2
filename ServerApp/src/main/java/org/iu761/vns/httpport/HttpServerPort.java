package org.iu761.vns.httpport;

import java.util.concurrent.*;
import java.io.*;
import java.net.*;

import org.apache.http.protocol.*;
import org.apache.http.*;
import org.apache.http.impl.*;

import org.iu761.vns.*;
import org.iu761.vns.logger.LogMessage;

public class HttpServerPort extends ServerPort {

    private HttpProcessor httpProc;
    private UriHttpRequestHandlerMapper reqHandMapper;
    private HttpService httpService;
    private final HttpConnectionFactory<DefaultBHttpServerConnection> connFactory;
    private StringBuilder currNameOfClient = new StringBuilder();

    public HttpServerPort(int portNumber, ConcurrentMap<String, ClientData> clientMesInfo) {
        super(portNumber, clientMesInfo);
        httpProc = HttpProcessorBuilder.create()
                .add(new ResponseDate())
                .add(new ResponseServer("Test/1.1"))
                .add(new ResponseContent())
                .add(new ResponseConnControl()).build();
        this.connFactory = DefaultBHttpServerConnectionFactory.INSTANCE;
        reqHandMapper = new UriHttpRequestHandlerMapper();
        reqHandMapper.register("*/connect", new ConnectToServerReqHandler(currNameOfClient,
                getClientMesInfo(), null));
        reqHandMapper.register("*/disconnect", new DisconnectReqHandler(getClientMesInfo()));
        reqHandMapper.register("*/downloadable", new ListOfDownloadableFilesReqHandler(getClientMesInfo()));
        reqHandMapper.register("*/uploaded", new ListOfUploadedFilesReqHandler());
        reqHandMapper.register("*/clients", new ListOfClientsReqHandler(getClientMesInfo()));
        reqHandMapper.register("*", new HttpFileReqHandler(getClientMesInfo()));
        httpService = new HttpService(httpProc, reqHandMapper);
    }

    private boolean connectToServer(HttpServerConnection conn) {
        boolean result = true;
        try {
            HttpContext context = new BasicHttpContext(null);
            httpService.handleRequest(conn, context);
        } catch (Exception error) {
            try {
                conn.shutdown();
            } catch (Exception err) {
                System.err.println(err.getMessage());
            }
            result = false;
        }
        return result;
    }



    @Override
    public void run() {
        super.run();
        try (ServerSocket serverSocket = new ServerSocket(getPortNumber())) {
            while (isListening()) {
                Socket socket = serverSocket.accept();
                System.out.println("Incoming connection from " + socket.getInetAddress());
                HttpServerConnection conn = this.connFactory.createConnection(socket);
                if(connectToServer(conn)) {
                    String name = currNameOfClient.toString();
                    currNameOfClient.delete(0, currNameOfClient.length());
                    synchronized (getLogger()) {
                        getLogger().log(LogMessage.CLIENT_CONNECT, "Client \"" + name + "\" connected to server.");
                    }
                    getExecClients().execute(new HttpServerClient(name, getClientMesInfo(), httpService, conn, getLogger()));
                }
            }
	    } catch (IOException error) {
            System.err.println("Could not listen on port " + getPortNumber());
        } catch (Exception error) {
            System.err.println(error.getMessage());
        }
    }
}
