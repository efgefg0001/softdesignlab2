package org.iu761.vns.httpport;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.*;

import org.apache.http.*;
import org.apache.http.entity.*;
import org.apache.http.protocol.*;
import org.apache.http.util.*;
import org.apache.commons.lang3.*;

import org.iu761.vns.ClientData;
import org.iu761.vns.utils.*;

class ListOfDownloadableFilesReqHandler implements HttpRequestHandler {

    private ConcurrentMap<String, ClientData> clientMesInfo;

    public ListOfDownloadableFilesReqHandler(ConcurrentMap<String, ClientData> clientMesInfo) {
        this.clientMesInfo = clientMesInfo;
    }

    @Override
    public void handle(HttpRequest request, HttpResponse httpResponse, HttpContext httpContext) throws HttpException, IOException {
        Header header = request.getFirstHeader("name");
        String name = header.getValue();
        if (clientMesInfo.containsKey(name)) {
            ArrayList<String> downloadable = new ArrayList<>();
            List<Message> downloads = clientMesInfo.get(name).getDownloadable();
            synchronized (downloads) {
                for (Message mes : downloads)
                    downloadable.add(mes.getName());
            }
            HttpEntity respEntity = new ByteArrayEntity(
                    SerializationUtils.serialize(downloadable),
                    ContentType.APPLICATION_OCTET_STREAM
            );
            httpResponse.setEntity(respEntity);
            httpResponse.setStatusCode(HttpStatus.SC_OK);
        } else {
            httpResponse.setStatusCode(HttpStatus.SC_METHOD_FAILURE);
            String resp = "There is no client with such name connected to the server.";
            httpResponse.setEntity(new StringEntity(resp));
        }
    }
}
