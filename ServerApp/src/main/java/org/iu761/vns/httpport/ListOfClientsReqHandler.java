package org.iu761.vns.httpport;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentMap;

import org.apache.http.*;
import org.apache.http.util.*;
import org.apache.http.protocol.*;
import org.apache.http.entity.*;
import org.apache.commons.lang3.*;

import org.iu761.vns.*;
import org.iu761.vns.utils.*;
import org.iu761.vns.crypto.base.Key;

class ListOfClientsReqHandler implements HttpRequestHandler {

    private ConcurrentMap<String, ClientData> clientMesInfo;

    public ListOfClientsReqHandler(ConcurrentMap<String, ClientData> clientMesInfo) {
        this.clientMesInfo = clientMesInfo;
    }

    @Override
    public void handle(HttpRequest request, HttpResponse httpResponse, HttpContext httpContext) throws HttpException, IOException {
                    HashMap<String, Key> clientsWithPublKeys= new HashMap<>();
        synchronized (clientMesInfo) {
            for(String client : clientMesInfo.keySet())
                clientsWithPublKeys.put(client, clientMesInfo.get(client).getPublicKey());
        }
        HttpEntity entity = new ByteArrayEntity(
                SerializationUtils.serialize(clientsWithPublKeys),
                ContentType.APPLICATION_OCTET_STREAM
        );
        httpResponse.setEntity(entity);
        httpResponse.setStatusCode(HttpStatus.SC_OK);
    }
}
