package org.iu761.vns.httpport;

import java.io.*;
import java.util.*;
import java.net.*;
import java.nio.charset.Charset;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentMap;

import org.apache.http.*;
import org.apache.http.protocol.*;
import org.apache.http.entity.*;
import org.apache.http.util.*;
import org.apache.commons.lang3.SerializationUtils;

import org.iu761.vns.*;
import org.iu761.vns.utils.Message;


class HttpFileReqHandler implements HttpRequestHandler {

    private ConcurrentMap<String, ClientData> clientMesInfo;

    public HttpFileReqHandler(ConcurrentMap<String, ClientData> clientMesInfo) {
        this.clientMesInfo = clientMesInfo;
    }

    @Override
    public void handle(HttpRequest request, HttpResponse response, HttpContext context)
            throws HttpException, IOException {
        System.out.println(this.getClass().getName());
        String method = request.getRequestLine().getMethod().toUpperCase(Locale.ENGLISH);
        if(method.equals("GET")) {
            String mesName = request.getRequestLine().getUri().substring(1);
            Header header = request.getFirstHeader("name");
            String clientName = header.getValue();
            Message result = clientMesInfo.get(clientName).getDownloadable()
                    .stream()
                    .filter(message-> message.getName().equals(mesName))
                    .findFirst()
                    .get();
            response.setEntity(new ByteArrayEntity(
                            SerializationUtils.serialize(result),
                            ContentType.APPLICATION_OCTET_STREAM)
            );
            response.setStatusCode(HttpStatus.SC_OK);
            clientMesInfo.get(clientName).getDownloadable().remove(result);
        } else if(method.equals("POST")) {
            if (request instanceof HttpEntityEnclosingRequest) {
                HttpEntity entity = ((HttpEntityEnclosingRequest) request).getEntity();
                Message message = (Message)SerializationUtils.deserialize(
                    EntityUtils.toByteArray(entity)
                );
                String receiver = message.getReceiver();
                clientMesInfo.get(receiver).getDownloadable().add(message);
            }
        } else
            throw new MethodNotSupportedException(method + " method not supported");
    }
}
