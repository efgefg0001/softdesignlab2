package org.iu761.vns.httpport;

import javafx.util.Pair;
import org.apache.http.*;
import org.apache.http.protocol.*;

import java.net.*;
import java.io.*;
import java.util.concurrent.*;
import java.util.*;

import org.iu761.vns.ClientData;
import org.iu761.vns.ServerClient;
import org.iu761.vns.logger.*;

class HttpServerClient extends ServerClient {

    private ConcurrentMap<String, ClientData> clients = null;
    private HttpService httpService;
    private HttpServerConnection conn;
    private String name;
    private Logger logger;

    public HttpServerClient(String name, ConcurrentMap<String, ClientData> clients,
                            HttpService httpService,
                            HttpServerConnection conn, Logger logger) {
        this.name = name;
        this.clients = clients;
        this.httpService = httpService;
        this.conn = conn;
        this.logger = logger;
    }

    @Override
    public void run() {
        try {
            System.out.println("New connection thread");
            HttpContext context = new BasicHttpContext(null);
            ClientData clientData = clients.get(name);
            while (/*!Thread.interrupted() &&*/
                    clientData.shouldListen() && this.conn.isOpen()) {
                logger.log(LogMessage.SEND_INFO, "Got message from client \"" + name + "\"");
                httpService.handleRequest(this.conn, context);
                logger.log(LogMessage.SEND_INFO, "Sent response to client \"" + name + "\"");
            }

        } catch (ConnectionClosedException ex) {
            System.err.println("Client closed connection");
        } catch (IOException error) {
            try {
                logger.log(LogMessage.SEND_ERROR, error.getMessage());
            } catch (Exception ex) {}
//            System.err.println("I/O error: " + ex.getMessage());
        } catch (HttpException error) {
            try {
                logger.log(LogMessage.SEND_ERROR, error.getMessage());
            } catch (Exception ex) {}
//            System.err.println("Unrecoverable HTTP protocol violation: " + error.getMessage());
        } catch (Exception error) {
            error.printStackTrace();
        } finally {
            clients.remove(name);
            try {
                this.conn.shutdown();
            } catch (IOException ignore) {}
            try {
                logger.log(LogMessage.CLIENT_DISCONNECT, "Client \"" + this.name +"\" disconnected.");
            } catch (Exception error) {}
        }
    }
}
