package org.iu761.vns;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.iu761.vns.httpport.HttpServerPort;
import org.iu761.vns.rawtcpport.RawTcpServerPort;
import org.iu761.vns.logger.*;

public class Main {
    private static final int HTTP_PORT = 4444;
    private static final int RAW_TCP_PORT = 4445;
    private static Map<Integer, ServerPort> createPorts(ConcurrentMap<String, ClientData> clientMesInfo) {
        Map<Integer, ServerPort> ports = new HashMap<>();
        ports.put(HTTP_PORT, new HttpServerPort(HTTP_PORT, clientMesInfo));
        ports.put(RAW_TCP_PORT, new RawTcpServerPort(RAW_TCP_PORT, clientMesInfo));
        return ports;
    }
    public static void main(String[] args) throws Exception {
        Server server = new Server(createPorts(new ConcurrentHashMap<>()));
        server.loop();
    }
}
