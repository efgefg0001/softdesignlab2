package org.iu761.vns.crypto.cipher;


import org.iu761.vns.crypto.base.Key;


import java.security.NoSuchAlgorithmException;
import java.security.InvalidKeyException;

import java.security.spec.KeySpec;

import java.security.spec.InvalidKeySpecException;


import javax.crypto.Cipher;

import javax.crypto.spec.DESKeySpec;

import javax.crypto.SecretKeyFactory;

import javax.crypto.IllegalBlockSizeException;
import javax.crypto.BadPaddingException;


public final class JavaDESSymmetricCipher extends AbstractJavaCipher {
    public JavaDESSymmetricCipher (String anAlgorithm, String aMode, String aPadding)
    {
        super(anAlgorithm, aMode, aPadding);
    }

    public byte[] encrypt (byte[] aContent, Key aKey)
    {
        try {
            KeySpec ks = new DESKeySpec(aKey.getEncodedKey());

            SecretKeyFactory skf = SecretKeyFactory.getInstance(aKey.getAlgorithm());
            java.security.Key key = skf.generateSecret(ks);

            Cipher cipher = getCipher();

            cipher.init(Cipher.ENCRYPT_MODE, key);

            return cipher.doFinal(aContent);
        } catch (NoSuchAlgorithmException e) {
            System.out.println("NoSuchAlgorithmException:\n" + e.getMessage());
            return null;
        } catch (InvalidKeySpecException e) {
            System.out.println("InvalidKeySpecException:\n" + e.getMessage());
            return null;
        } catch (InvalidKeyException e) {
            System.out.println("InvalidKeyException:\n" + e.getMessage());
            return null;
        } catch (IllegalBlockSizeException e) {
            System.out.println("IllegalBlockSizeException:\n" + e.getMessage());
            return null;
        } catch (BadPaddingException e) {
            System.out.println("BadPaddingException:\n" + e.getMessage());
            return null;
        } 
    }

    public byte[] decrypt (byte[] aContent, Key aKey)
    {
        try {
            KeySpec ks = new DESKeySpec(aKey.getEncodedKey());
            SecretKeyFactory skf = SecretKeyFactory.getInstance(aKey.getAlgorithm());
            java.security.Key key = skf.generateSecret(ks);

            Cipher cipher = getCipher();

            cipher.init(Cipher.DECRYPT_MODE, key);

            return cipher.doFinal(aContent);
        } catch (NoSuchAlgorithmException e) {
            System.out.println("NoSuchAlgorithmException:\n" + e.getMessage());
            return null;
        } catch (InvalidKeySpecException e) {
            System.out.println("InvalidKeySpecException:\n" + e.getMessage());
            return null;
        } catch (InvalidKeyException e) {
            System.out.println("InvalidKeyException:\n" + e.getMessage());
            return null;
        } catch (IllegalBlockSizeException e) {
            System.out.println("IllegalBlockSizeException:\n" + e.getMessage());
            return null;
        } catch (BadPaddingException e) {
            System.out.println("BadPaddingException:\n" + e.getMessage());
            return null;
        } 
    }   
}