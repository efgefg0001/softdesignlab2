package org.iu761.vns.crypto.cipher;


import org.iu761.vns.crypto.base.Key;


import javax.crypto.Cipher;

import java.security.spec.EncodedKeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;


import java.security.KeyFactory;

import java.security.NoSuchAlgorithmException;
import java.security.InvalidKeyException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.IllegalBlockSizeException;
import javax.crypto.BadPaddingException;



public final class JavaAsymmetricCipher extends AbstractJavaCipher {
    public JavaAsymmetricCipher (String anAlgorithm, String aMode, String aPadding)
    {
        super(anAlgorithm, aMode, aPadding);
    }

    public byte[] encrypt (byte[] aContent, Key aKey)
    {
        try {
            EncodedKeySpec eks = new X509EncodedKeySpec(aKey.getEncodedKey());
            KeyFactory kf = KeyFactory.getInstance(aKey.getAlgorithm());
            java.security.Key pk = kf.generatePublic(eks);

            Cipher cipher = getCipher();

            cipher.init(Cipher.ENCRYPT_MODE, pk);

            return cipher.doFinal(aContent);
        } catch (NoSuchAlgorithmException e) {
            System.out.println("NoSuchAlgorithmException:\n" + e.getMessage());
            return null;
        } catch (InvalidKeySpecException e) {
            System.out.println("InvalidKeySpecException:\n" + e.getMessage());
            return null;
        } catch (InvalidKeyException e) {
            System.out.println("InvalidKeyException:\n" + e.getMessage());
            return null;
        } catch (IllegalBlockSizeException e) {
            System.out.println("IllegalBlockSizeException:\n" + e.getMessage());
            return null;
        } catch (BadPaddingException e) {
            System.out.println("BadPaddingException:\n" + e.getMessage());
            return null;
        } 
    }

    public byte[] decrypt (byte[] aContent, Key aKey)
    {
        try {
            EncodedKeySpec eks = new PKCS8EncodedKeySpec(aKey.getEncodedKey());
            KeyFactory kf = KeyFactory.getInstance(aKey.getAlgorithm());
            java.security.Key pk = kf.generatePrivate(eks);

            Cipher cipher = getCipher();

            cipher.init(Cipher.DECRYPT_MODE, pk);

            return cipher.doFinal(aContent);
        } catch (NoSuchAlgorithmException e) {
            System.out.println("NoSuchAlgorithmException:\n" + e.getMessage());
            return null;
        } catch (InvalidKeySpecException e) {
            System.out.println("InvalidKeySpecException:\n" + e.getMessage());
            return null;
        } catch (InvalidKeyException e) {
            System.out.println("InvalidKeyException:\n" + e.getMessage());
            return null;
        } catch (IllegalBlockSizeException e) {
            System.out.println("IllegalBlockSizeException:\n" + e.getMessage());
            return null;
        } catch (BadPaddingException e) {
            System.out.println("BadPaddingException:\n" + e.getMessage());
            return null;
        } 
    }   
}