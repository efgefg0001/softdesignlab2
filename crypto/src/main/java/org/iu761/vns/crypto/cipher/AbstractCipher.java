package org.iu761.vns.crypto.cipher;

public abstract class AbstractCipher implements Cipher {
    private String algorithm;
    private String mode;
    private String padding;

    public AbstractCipher (String anAlgorithm, String aMode, String aPadding)
    {
        this.algorithm = anAlgorithm;
        this.mode      = aMode;
        this.padding   = aPadding;
    }
}