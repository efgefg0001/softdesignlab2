package org.iu761.vns.crypto.cipher;


import java.security.NoSuchAlgorithmException;


import javax.crypto.Cipher;

import javax.crypto.NoSuchPaddingException;


public abstract class AbstractJavaCipher extends AbstractCipher {
    private Cipher cipher;

    public AbstractJavaCipher (String anAlgorithm, String aMode, String aPadding)
    {
        super(anAlgorithm, aMode, aPadding);

        try {
            this.cipher = Cipher.getInstance(anAlgorithm + "/" + aMode + "/" + aPadding);
        } catch (NoSuchAlgorithmException e) {
            System.out.println("NoSuchAlgorithmException:\n" + e.getMessage());
        } catch (NoSuchPaddingException e) {
            System.out.println("NoSuchPaddingException:\n" + e.getMessage());
        }
    }

    public Cipher getCipher () { return cipher; }
}