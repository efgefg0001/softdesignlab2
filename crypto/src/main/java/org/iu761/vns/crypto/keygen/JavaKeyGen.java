package org.iu761.vns.crypto.keygen;


import org.iu761.vns.crypto.base.Key;
import org.iu761.vns.crypto.base.KeyPair;


import javax.crypto.KeyGenerator;

import java.security.KeyPairGenerator;

import java.security.NoSuchAlgorithmException;


public class JavaKeyGen implements KeyGen {
    public Key genSymmetricKey (String anAlgorithm, int aLengthKey)
    {
        try {
            KeyGenerator keygen = KeyGenerator.getInstance(anAlgorithm);
            keygen.init(aLengthKey);

            java.security.Key key = keygen.generateKey();

            return new Key(key.getEncoded(), anAlgorithm); 
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    public KeyPair genAsymmetricKey (String anAlgorithm, int aLengthKey)
    {
        try {
            KeyPairGenerator keygen = KeyPairGenerator.getInstance(anAlgorithm);
            keygen.initialize(aLengthKey);
    
            java.security.KeyPair key = keygen.generateKeyPair();

            return new KeyPair(new Key(key.getPublic().getEncoded(), anAlgorithm),
                               new Key(key.getPrivate().getEncoded(), anAlgorithm),
                               anAlgorithm);
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }
}