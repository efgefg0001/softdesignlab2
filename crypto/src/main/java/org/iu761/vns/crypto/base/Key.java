package org.iu761.vns.crypto.base;

import java.io.Serializable;

public final class Key implements Serializable{
    private String algorithm;
    private byte[] rawKey;

    public Key (byte[] aRAWKey, String anAlgorithm)
    {
        this.algorithm = anAlgorithm;
        this.rawKey = aRAWKey;
    }

    public String getAlgorithm  () { return this.algorithm;  }
    public byte[] getEncodedKey () { return this.rawKey;     }
} 