package org.iu761.vns.crypto.keygen;

import java.util.Map;
import java.util.HashMap;

public class KeyGenFactory {
    private static Map <String, KeyGen> keygens = new HashMap<>();

    static 
    {
        keygens.put("java", new JavaKeyGen());
        // put some another lib keygen
    }

    public static KeyGen getInstance (String aType)
    {
        return keygens.get(aType);
    }
}