package org.iu761.vns.crypto.keygen;

import org.iu761.vns.crypto.base.Key;
import org.iu761.vns.crypto.base.KeyPair;

public interface KeyGen {
    Key     genSymmetricKey  (String anAlgorithm, int aLengthKey);
    KeyPair genAsymmetricKey (String anAlgorithm, int aLengthKey);
}