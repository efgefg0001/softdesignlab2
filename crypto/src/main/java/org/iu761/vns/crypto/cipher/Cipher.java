package org.iu761.vns.crypto.cipher;

import org.iu761.vns.crypto.base.Key;

public interface Cipher {
    byte[] encrypt (byte[] aContent, Key aKey);
    byte[] decrypt (byte[] aContent, Key aKey);
}