package org.iu761.vns.crypto.base;

public final class KeyPair {
    private final String algorithm;
    private final Key    publicKey;
    private final Key    privateKey;

    public KeyPair (Key aPublicKey, Key aPrivateKey, String anAlgorithm)
    {
        this.publicKey  = aPublicKey;
        this.privateKey = aPrivateKey;
        this.algorithm  = anAlgorithm;
    }

    public String getAlgorithm  () { return this.algorithm;  }
    public Key    getPublicKey  () { return this.publicKey;  }
    public Key    getPrivateKey () { return this.privateKey; }
}