package org.iu761.vns;


import org.iu761.vns.crypto.keygen.KeyGenFactory;
import org.iu761.vns.crypto.keygen.KeyGen;
import org.iu761.vns.crypto.base.Key;
import org.iu761.vns.crypto.base.KeyPair;

import org.iu761.vns.crypto.cipher.AbstractCipher;
import org.iu761.vns.crypto.cipher.JavaDESSymmetricCipher;
import org.iu761.vns.crypto.cipher.JavaAsymmetricCipher;


public class Main {
    public static void main(String[] args)
    {
        String msg = "Hello World";
        
        String symAlgorithm = "DES";
        String symMode      = "ECB";
        String symPadding   = "PKCS5Padding";
        int    symLength    = 56;

        String asymAlgorithm = "RSA";
        String asymMode      = "ECB";
        String asymPadding   = "PKCS1Padding";
        int    asymLength    = 1024;

        KeyGen keygen = KeyGenFactory.getInstance("java");
        // тут бы фабрику для шифровальщиков бы по хорошему

        Key     symkey  = keygen.genSymmetricKey(symAlgorithm, symLength);
        KeyPair asymkey = keygen.genAsymmetricKey(asymAlgorithm, asymLength);

        AbstractCipher rsaCipher = new JavaAsymmetricCipher(asymAlgorithm, asymMode, asymPadding);
        AbstractCipher desCipher = new JavaDESSymmetricCipher(symAlgorithm, symMode, symPadding);

        // клиенты обмениваются KeyPait.getPublicKey() которыми шифруют симметричный ключ
        // сначало передаётся зашфированный симметричный ключ, чтобы поддержать возможность докачки
        // шифруем симметричный ключ публичным ключом пользователя которому передаём

        byte[] rawEncryptedSymKey = rsaCipher.encrypt(symkey.getEncodedKey(), asymkey.getPublicKey());

        // передаём зашифрованный симметричный ключ пользователю который должен получить сообщение
        
        // User2 расшифровывает

        byte[] rawDecryptedSymKey = rsaCipher.decrypt(rawEncryptedSymKey, asymkey.getPrivateKey());

        // после чего симметричным ключом шифруется сообщение (User1)
        // не успел добавить асбтрактную фабрику для шифровальщиков, мой косяк

        byte[] rawEncryptedMsg = desCipher.encrypt(msg.getBytes(), symkey);

        // User2 получив зашифрованное сообщение, расшифровывает

        byte[] rawDecryptedMsg = desCipher.decrypt(rawEncryptedMsg, new Key(rawDecryptedSymKey, symAlgorithm));

        // Итого

        String decryptedMsg = new String(rawDecryptedMsg);

        System.out.println(decryptedMsg);        
    }
}