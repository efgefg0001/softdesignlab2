package org.iu761.vns;

import java.util.Map.Entry;
import java.io.*;
import java.util.*;

import org.iu761.vns.utils.Message;
import org.iu761.vns.crypto.base.Key;

public interface Messenger extends AutoCloseable {
    void connectToHost(String name, String host, int portNumber, Key publicKey) throws Exception;
    void disconnect() throws Exception;
    void sendMessage(Message message) throws Exception;
    Map<String, Key> getConnectedClientsWithPublKeys() throws Exception;
    List<String> getListOfDownloadableMessages() throws Exception;
    Message receiveMessage(String name) throws Exception;
}
