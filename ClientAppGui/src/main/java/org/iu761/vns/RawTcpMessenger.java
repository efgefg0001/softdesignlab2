package org.iu761.vns;

import java.io.*;
import java.util.*;
import java.net.*;

import org.apache.commons.lang3.SerializationUtils;
import org.iu761.vns.crypto.base.Key;
import org.iu761.vns.utils.*;

public class RawTcpMessenger implements Messenger {

    private Socket socket;
    private String name;
    private String host;
    private int port;

    public RawTcpMessenger() {
    }

    @Override
    public void connectToHost(String name, String host, int portNumber, Key publicKey) throws Exception {
        this.name = name;
        this.host = host;
        this.port = portNumber;
        socket = new Socket(host, portNumber);
        ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
        ObjectInputStream input = new ObjectInputStream(socket.getInputStream());
        Message request = new Message(
                name,
                "server",
                SerializationUtils.serialize(publicKey),
                "connect");
        output.writeObject(request);
        Message response = (Message)input.readObject();
        if(response.getError() != null)
            throw response.getError();
    }

    @Override
    public void sendMessage(Message message) throws Exception {
        ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
        ObjectInputStream input = new ObjectInputStream(socket.getInputStream());
        output.writeObject(message);
        Message response = (Message)input.readObject();
        if(response.getError() != null)
            throw response.getError();
    }

    @Override
    public Message receiveMessage(String nameOfMes) throws Exception {
        ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
        ObjectInputStream input = new ObjectInputStream(socket.getInputStream());
        Message request = new Message(name, "server", nameOfMes.getBytes(), "download");
        output.writeObject(request);
        Message response = (Message)input.readObject();
        if(response.getError() != null)
            throw  response.getError();
        return response;
    }

    @Override
    public void disconnect() throws Exception {
        ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
        ObjectInputStream input = new ObjectInputStream(socket.getInputStream());
        Message request = new Message(name, "server", new byte[0], "disconnect");
        output.writeObject(request);
        Message response = (Message)input.readObject();
        if(response.getError() != null)
            throw  response.getError();
        input.close();
        output.close();
        socket.close();
    }

    @Override
    public void close() throws Exception {
        disconnect();
    }

    @Override
    public Map<String, Key> getConnectedClientsWithPublKeys() throws Exception {
        Map<String, Key> result = new HashMap<>();
        ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
        ObjectInputStream input = new ObjectInputStream(socket.getInputStream());
        Message request = new Message(
                name,
                "server",
                "clients".getBytes(),
                "clients");
        output.writeObject(request);
        Message response = (Message)input.readObject();
        if(response.getError() != null)
            throw response.getError();
        result = (HashMap<String, Key>)SerializationUtils.deserialize(response.getContent());
        return result;
    }

    @Override
    public List<String> getListOfDownloadableMessages() throws Exception {
        List<String> result = new ArrayList<>();
        ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
        ObjectInputStream input = new ObjectInputStream(socket.getInputStream());
        Message request = new Message(
                name,
                "server",
                "downloadable".getBytes(),
                "downloadable");
        output.writeObject(request);
        Message response = (Message)input.readObject();
        if(response.getError() != null)
            throw response.getError();
        result = (ArrayList<String>)SerializationUtils.deserialize(response.getContent());
        return result;
    }
}
