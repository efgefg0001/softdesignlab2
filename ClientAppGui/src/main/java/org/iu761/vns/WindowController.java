package org.iu761.vns;

import java.nio.file.*;
import java.io.*;
import java.net.*;
import java.util.*;

import javafx.collections.*;
import javafx.fxml.*;
import javafx.util.*;
import javafx.scene.layout.*;
import javafx.stage.*;
import javafx.scene.control.*;
import javafx.event.*;
import org.apache.commons.lang3.SerializationUtils;
import org.controlsfx.dialog.*;

import org.iu761.vns.utils.Message;
import org.iu761.vns.exceptions.*;
import org.iu761.vns.logger.*;
import org.iu761.vns.crypto.keygen.*;
import org.iu761.vns.crypto.base.*;
import org.iu761.vns.crypto.cipher.*;

public class WindowController implements Initializable, AutoCloseable {

    @FXML
    private TextField nameTextField;

    @FXML
    private TextField hostTextField;

    @FXML
    private Button connectButton;

    @FXML
    private TextField portTextField;

    @FXML
    private Button disconnectButton;

    @FXML
    private Button downloadButton;

    @FXML
    private Button browseButton;

    @FXML
    private Button sendButton;

    @FXML
    private ComboBox<String> protocolsComboBox;

    @FXML
    private GridPane connectedGridPane;

    @FXML
    private GridPane disconnectedGridPane;

    @FXML
    private ListView<String> clientsListView;

    @FXML
    private ListView<String> downloadableListView;

    @FXML
    private Button updateButton;

    @FXML
    private TextField currentFileTextField;

    // шифрование
    String symAlgorithm = "DES";
    String symMode      = "ECB";
    String symPadding   = "PKCS5Padding";
    int    symLength    = 56;
    String asymAlgorithm = "RSA";
    String asymMode      = "ECB";
    String asymPadding   = "PKCS1Padding";
    int    asymLength    = 1024;
    KeyGen keyGen;
    Key symKey;
    KeyPair asymKey;
    AbstractCipher rsaCipher;
    AbstractCipher desCipher;

    // логгер
    private Logger logger = Logger.getInstance(Type.CLIENT);

    private Messenger mes;
    private Stage stage;
    private File currentFile;
    private String baseLocalStorageName = "downloads";
    private String localStorageName;
    private Map<String, Messenger> protocols = createProtocols();
    private Map<String, Key> clientsWithPubKeys = new HashMap<>();

    private Map<String, Messenger> createProtocols() {
        Map<String, Messenger> map = new HashMap<>();
        map.put("Http", new HttpMessenger());
        map.put("Raw Tcp", new RawTcpMessenger());
        return map;
    }

    private void initEncryption() {
        keyGen = KeyGenFactory.getInstance("java");
        symKey  = keyGen.genSymmetricKey(symAlgorithm, symLength);
        asymKey = keyGen.genAsymmetricKey(asymAlgorithm, asymLength);
        System.out.println("AsymKey = private: " + asymKey.getPrivateKey().getEncodedKey() + ", public: " + asymKey.getPublicKey().getEncodedKey());
        rsaCipher = new JavaAsymmetricCipher(asymAlgorithm, asymMode, asymPadding);
        desCipher = new JavaDESSymmetricCipher(symAlgorithm, symMode, symPadding);
    }

    @Override
    public void initialize(URL url, ResourceBundle res) {
        protocolsComboBox.getItems().setAll(protocols.keySet());
        protocolsComboBox.setValue("Http");
        initEncryption();
        try {
            logger.log(LogMessage.START, "Client started.");
        } catch (Exception error) {
            error.printStackTrace();
        }
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @Override
    public void close() throws Exception {
        logger.log(LogMessage.STOP, "Client closed.");
        for(Messenger item : protocols.values())
            item.close();

    }

   private void createLocalStorage() {
       localStorageName = baseLocalStorageName + "_" + nameTextField.getText();
       File storage = new File(localStorageName);
       if(storage.exists() && !storage.isDirectory())
           storage.delete();
       if (!storage.exists())
           storage.mkdir();
   }
    private boolean canTakeThisName(String word) {
        boolean result = true;
        if(word.trim().toLowerCase().equals("server"))
            result = false;
        return result;
    }

    @FXML
    void onActionConnectButton(ActionEvent event) {
        try {
            if(!canTakeThisName(nameTextField.getText()))
                throw new CannotTakeNameException(nameTextField.getText());
            mes = protocols.get(protocolsComboBox.getValue());
            mes.connectToHost(
                    nameTextField.getText(),
                    hostTextField.getText(),
                    Integer.parseInt(portTextField.getText()),
                    this.asymKey.getPublicKey()
            );

            connectedGridPane.setDisable(false);
            disconnectedGridPane.setDisable(true);
            createLocalStorage();
//            onActionUpdateButton(null);
        } catch(Exception error) {
            Dialogs.create()
                    .owner(stage)
                    .title("Error")
                    .message(error.getMessage())
                    .showError();
        }

    }

    @FXML
    void onActionDisconnectButton(ActionEvent event) {
        try {
            protocols.get(protocolsComboBox.getValue()).disconnect();
            clientsListView.getItems().clear();
            downloadableListView.getItems().clear();
            currentFileTextField.clear();
            connectedGridPane.setDisable(true);
            disconnectedGridPane.setDisable(false);
        } catch (Exception error) {
            Dialogs.create()
                    .owner(stage)
                    .title("Error")
                    .message(error.getMessage())
                    .showError();
        }
    }

    private byte[] getDecryptedContent(Pair<byte[], byte[]> content) {
        byte[] encrKey = content.getKey();
        byte[] rawDecryptedSymKey = rsaCipher.decrypt(encrKey, asymKey.getPrivateKey());
        byte[] rawDecryptedMsg = desCipher.decrypt(content.getValue(), new Key(rawDecryptedSymKey, symAlgorithm));
        return rawDecryptedMsg;
    }

    @FXML
    void onActionDownloadButton(ActionEvent event) {
        try {
            String name = downloadableListView.getSelectionModel().getSelectedItem();
            if(name == null)
                throw new Exception("You haven't selected message for downloading.");
            logger.log(LogMessage.SEND_START, "Started downloading message \"" + name +"\".");
            Message message = mes.receiveMessage(name);
            logger.log(LogMessage.SEND_START, "Received message \"" + name +"\".");
            File saveTo = new File(localStorageName + "/" + message.getName());
            logger.log(LogMessage.SEND_START, "Wrote downloaded message \"" + name +"\". in file \"" + saveTo.getName());

            Pair<byte[], byte[]> content = (Pair<byte[], byte[]>)SerializationUtils.deserialize(message.getContent());

            Files.write(Paths.get(saveTo.toURI()),
                    getDecryptedContent(content)
            );
            Dialogs.create().owner(stage)
                    .title("Information")
                    .message("Message \"" + message.getName()+  "\" has received.")
                    .showInformation();
            onActionUpdateButton(null);
        } catch (Exception error) {
            try {
                logger.log(LogMessage.SEND_ERROR, error.getMessage());
            } catch(Exception ex) {}
            Dialogs.create()
                    .owner(stage)
                    .title("Error")
                    .message(error.getMessage())
                    .showError();
        }
    }

    @FXML
    void onActionBrowseButton(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose file");
        currentFile = fileChooser.showOpenDialog(stage);
        currentFileTextField.setText(currentFile.getAbsolutePath());
    }

    private Pair<byte[], byte[]> getEncryptedMessage(String reciever, byte[] content) {
        byte[] rawEncryptedSymKey = rsaCipher.encrypt(symKey.getEncodedKey(), clientsWithPubKeys.get(reciever));
        byte[] rawEncryptedMsg = desCipher.encrypt(content, symKey);
        return new Pair<>(rawEncryptedSymKey, rawEncryptedMsg);
    }

    @FXML
    void onActionSendButton(ActionEvent event) {
        try {
            String receiver = clientsListView.getSelectionModel().getSelectedItem();
            if(receiver == null)
                throw new Exception("You haven't selected receiver.");
            if(receiver.equals(nameTextField.getText()))
                throw new Exception("You can't send message to yourself.");
            byte[] content = Files.readAllBytes(currentFile.toPath());

            Pair<byte[], byte[]> encrKeyAndContent = getEncryptedMessage(receiver, content);

            Message message = new Message(
                    nameTextField.getText(),
                    receiver,
                    SerializationUtils.serialize(encrKeyAndContent),
                    currentFile.getName()
            );
            logger.log(LogMessage.SEND_START, "Client started sending message.");
            mes.sendMessage(message);
            Dialogs.create().owner(stage)
                    .title("Information")
                    .message("Message has sent")
                    .showInformation();
            logger.log(LogMessage.SEND_STOP, "Client ended sending message.");
//            this.onActionUpdateButton(null);
        } catch(Exception error) {
            try {
                logger.log(LogMessage.SEND_ERROR, error.getMessage());
            } catch (Exception ex) {}
            Dialogs.create()
                    .owner(stage)
                    .title("Error")
                    .message(error.getMessage())
                    .showError();
        }
    }

    @FXML
    void onActionUpdateButton(ActionEvent event) {
        try {
            clientsWithPubKeys = mes.getConnectedClientsWithPublKeys();
            ObservableList<String> clients = FXCollections.observableArrayList(clientsWithPubKeys.keySet());
            logger.log(LogMessage.SEND_INFO, "Updated information about connected clients.");
            ObservableList<String> downloadable = FXCollections.observableArrayList(mes.getListOfDownloadableMessages());
            logger.log(LogMessage.SEND_INFO, "Updated information about downloadable messages.");
            clientsListView.setItems(clients);
            downloadableListView.setItems(downloadable);
        } catch (Exception error) {
            try {
                logger.log(LogMessage.SEND_ERROR, error.getMessage());
            } catch (Exception ex) {}
            Dialogs.create()
                    .owner(stage)
                    .title("Error")
                    .message(error.getMessage())
                    .showError();
        }
    }
}
