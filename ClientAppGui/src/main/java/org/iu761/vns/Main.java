package org.iu761.vns;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    private String fxmlFile = "Window.fxml";
    private int width = 400;
    private int height = 500;
    public static void main(String[] args) {
        try {
            launch(args);
        } catch(Exception error) {
//            System.err.println(error.getMessage());
            error.printStackTrace();
        }
    }

    private Scene createScene() throws Exception {
		Parent rootNode = FXMLLoader.load(getClass().getResource(fxmlFile));
        Scene scene = new Scene(rootNode, width, height);
        return scene;
    }
    private FXMLLoader createLoader() {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(fxmlFile));
        return loader;
    }
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = createLoader();
        stage.setTitle("ClientAppGui");
        stage.setScene(new Scene(loader.load(), width, height));
        WindowController controller = loader.getController();
        controller.setStage(stage);
        stage.setOnCloseRequest((event) -> {
            try {
                controller.close();
            } catch (Exception error) {
            }
        });
        stage.show();

    }
}

