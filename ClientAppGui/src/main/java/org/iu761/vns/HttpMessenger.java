package org.iu761.vns;

import java.util.*;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.http.client.methods.*;
import org.apache.http.*;
import org.apache.http.entity.*;
import org.apache.http.impl.client.*;
import org.apache.http.util.*;

import org.iu761.vns.crypto.base.Key;
import org.iu761.vns.utils.*;

public class HttpMessenger implements Messenger {

    private String name;
    private String hostName;
    private int portNumber;
    private CloseableHttpClient httpClient;

    public HttpMessenger() {
    }

    private String getHttpUrlToHost() {
        return "http://" + hostName + ":" + portNumber;
    }

    @Override
    public void connectToHost(String name, String hostName, int portNumber, Key publicKey) throws Exception {
        httpClient = HttpClients.createDefault();
        this.name = name;
        this.hostName = hostName;
        this.portNumber = portNumber;
        HttpPost post = new HttpPost(getHttpUrlToHost() + "/connect");
        Message request = new Message(
                name,
                "server",
                SerializationUtils.serialize(publicKey),
                "connect");
        HttpEntity entity = new ByteArrayEntity(
                SerializationUtils.serialize(request),
                ContentType.APPLICATION_OCTET_STREAM
        );
//        HttpEntity nameEntity = new StringEntity(
//                name, ContentType.create("text/plain", Consts.UTF_8)
//        );
        post.setEntity(entity);
        CloseableHttpResponse response = httpClient.execute(post);
        if(response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
            String strResp = EntityUtils.toString(response.getEntity());
            throw new Exception(strResp);
        }
    }

    @Override
    public void sendMessage(Message message) throws Exception {
        HttpPost post = new HttpPost(getHttpUrlToHost() + "/" + message.getName());
        HttpEntity entity = new ByteArrayEntity(
                SerializationUtils.serialize(message),
                ContentType.APPLICATION_OCTET_STREAM
        );
        post.setEntity(entity);
        CloseableHttpResponse response = httpClient.execute(post);
    }

    @Override
    public Message receiveMessage(String mesName) throws Exception {
        HttpGet get = new HttpGet(getHttpUrlToHost() + "/" + mesName);
        get.addHeader("name", this.name);
        CloseableHttpResponse response = httpClient.execute(get);
        Message message = null;
        if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            byte[] bytes = EntityUtils.toByteArray(response.getEntity());
            message = (Message) SerializationUtils.deserialize(bytes);
        } else
            throw new Exception(response.getStatusLine().getReasonPhrase());
        return message;
    }

    @Override
    public void disconnect() throws Exception {
        HttpGet get = new HttpGet(getHttpUrlToHost() + "/disconnect");
        get.addHeader("name", this.name);
        CloseableHttpResponse response = httpClient.execute(get);
        if(response.getStatusLine().getStatusCode() != HttpStatus.SC_OK)
            throw new Exception("Something is wrong.");
        httpClient.close();
    }

    @Override
    public void close() throws Exception {
        disconnect();
    }

    @Override
    public Map<String, Key> getConnectedClientsWithPublKeys() throws Exception {
        HttpGet get = new HttpGet(getHttpUrlToHost() + "/clients");
        CloseableHttpResponse response = httpClient.execute(get);
        Map<String, Key> clients = new HashMap<>();
        if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            byte[] bytes = EntityUtils.toByteArray(response.getEntity());
            clients = (HashMap<String, Key>)SerializationUtils.deserialize(bytes);
        } else
            throw new Exception(response.getStatusLine().getReasonPhrase());
        return clients;
    }

    @Override
    public List<String> getListOfDownloadableMessages() throws Exception {
        HttpGet get = new HttpGet(getHttpUrlToHost() + "/downloadable");
        get.addHeader("name", name);
        CloseableHttpResponse response = httpClient.execute(get);
        List<String> downloadable = new ArrayList<>();
        if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            byte[] bytes = EntityUtils.toByteArray(response.getEntity());
            downloadable = (ArrayList<String>) SerializationUtils.deserialize(bytes);
        } else
            throw new Exception(response.getStatusLine().getReasonPhrase());
        return downloadable;
    }
}

