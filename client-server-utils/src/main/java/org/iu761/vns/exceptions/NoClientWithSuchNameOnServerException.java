package org.iu761.vns.exceptions;

public class NoClientWithSuchNameOnServerException extends Exception {
    public NoClientWithSuchNameOnServerException(String name) {
        super("There is no client with name \""+name+"\" connected to the server.");
    }

}
