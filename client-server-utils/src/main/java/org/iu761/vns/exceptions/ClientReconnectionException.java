package org.iu761.vns.exceptions;

public class ClientReconnectionException extends Exception{

    public ClientReconnectionException(String mes) {
        super(mes);
    }

    public ClientReconnectionException() {
        super("There is client connected to the server.");
    }
}


