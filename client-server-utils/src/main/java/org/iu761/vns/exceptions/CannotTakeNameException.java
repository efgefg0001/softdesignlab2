package org.iu761.vns.exceptions;

public class CannotTakeNameException extends Exception {

    public CannotTakeNameException(String name) {
        super("You can not take \"" + name + "\" as name.");
    }
}
