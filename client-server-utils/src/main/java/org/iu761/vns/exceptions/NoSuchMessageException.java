package org.iu761.vns.exceptions;

public class NoSuchMessageException extends Exception {
    public NoSuchMessageException(String name) {
        super("There is no message with name \"" + name +"\"");
    }
}
