package org.iu761.vns.utils;

import java.io.Serializable;

public class Message implements Serializable {

    private String sender;
    private String receiver;
    private byte[] content;
    private String name;
    private Exception error = null;

    public Message(String sender, String receiver, byte[] content, String name) {
        this.sender  = sender;
        this.receiver = receiver;
        this.content = content;
        this.name = name;
    }

    public Message(String sender, String receiver, byte[] content, String name, Exception error) {
        this.sender  = sender;
        this.receiver = receiver;
        this.content = content;
        this.name = name;
        this.error = error;
    }

    public Exception getError() {
        return error;
    }

    public String getSender() {
        return sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public byte[] getContent() {
        return content;
    }

    public String getName() {
        return name;
    }
}
