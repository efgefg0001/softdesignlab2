package org.iu761.vns.logger;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LoggerForServer implements AbstractLogger {
	@Override
    public synchronized boolean log(LogMessage mess, String comment) throws Exception{
        switch (mess)
        {
            case START:

                txt = "START: " + getFormat().format(getDate() ) + " ["+ comment+"]";
                update(getF(), txt);
                break;
            case SEND_START:
                txt = "SEND_START: " + getFormat().format(getDate() ) + " ["+ comment+"]";
                update(getF(), txt);
                break;
            case SEND_INFO:
                txt = "SEND_INFO: " + getFormat().format(getDate() ) + " ["+ comment+"]";
                update(getF(), txt);
                break;
            case SEND_STOP:
                txt = "SEND_STOP: " + getFormat().format(getDate() ) + " ["+ comment+"]";
                update(getF(), txt);
                break;
            case SEND_ERROR:
                txt = "SEND_ERROR: " + getFormat().format(getDate() ) + " ["+ comment+"]";
                update(getF(), txt);
                break;
            case CLIENT_CONNECT:
                txt = "CLIENT_CONNECT: " + getFormat().format(getDate() ) + " ["+ comment+"]";
                update(getF(), txt);
                break;
            case CLIENT_DISCONNECT:
                txt = "CLIENT_DISCONNECT: " + getFormat().format(getDate() ) + " ["+ comment+"]";
                update(getF(), txt);
                break;
            case STOP:
                txt =  "STOP: " + getFormat().format(getDate()) + " ["+ comment+"]";
                update(getF(), txt);
                f = null;
                break;
            default:
                return false;
        }
        return true;
	}

	@Override
	public synchronized String showMyHistory() throws FileNotFoundException {
		String resut = read(f);
		return resut;
	}


	public SimpleDateFormat getFormat() {
		return format;
	}

	public synchronized Date getDate() {
		return new Date();
	}

	public synchronized File getF() throws  Exception {
        if(f==null)
            f = new File("log.txt");
        if(!f.exists()){
            f.createNewFile();
        }
		return f;
	}

	public synchronized void setF(File f) {
		this.f = f;
	}

	private String txt;
	private SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy, hh:mm:ss");
	private Date d = new Date();
	private File f = null;

	public synchronized void write(File f, String txt) throws IOException {
		try {

		        PrintWriter out = new PrintWriter(f.getAbsoluteFile());
		        try {
		            out.print(txt);
		        } finally {
		            out.close();
		        }
		    } catch(IOException e) {

		        throw new RuntimeException(e);
		    }
		}

	public synchronized void update(File f, String txt) throws IOException {

	    exists(f);
	    StringBuilder sb = new StringBuilder();
	    String oldFile = read(f);
	    sb.append(oldFile);
	    sb.append(txt);
	    write(f, sb.toString());
	}

	public synchronized String read(File f) throws FileNotFoundException {
	    StringBuilder sb = new StringBuilder();
	    exists(f);
	    try {

	        BufferedReader in = new BufferedReader(new FileReader( f.getAbsoluteFile()));
	        try {
	            String s;
	            while ((s = in.readLine()) != null) {
	                sb.append(s);
	                sb.append("\n");
	            }
	        } finally {

	            in.close();
	        }
	    } catch(IOException e) {
	        throw new RuntimeException(e);
	    }

	    return sb.toString();
	}
	private synchronized void exists(File f) throws FileNotFoundException {
		f = new File("log.txt");
	    if (!f.exists()){
	        throw new FileNotFoundException(f.getName());
	    }

	}

}
