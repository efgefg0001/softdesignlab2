package org.iu761.vns.logger;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class LoggerForClient implements AbstractLogger {
	private ArrayList<String> list = new ArrayList<String>(); 
	private SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy, hh:mm:ss");
	private File f = null;
	@Override
	public synchronized boolean log(LogMessage mess, String comment) throws IOException {
		switch (mess)
		{
		case START:
			if (f == null)
			{
				f = new File("log.txt"); 
				if(!f.exists()){	
					f.createNewFile();
	        	}
				getList().add("START: " + getFormat().format(getDate()) + " ["+ comment+"]");
			}
			break;
		case SEND_START:
			if(f == null)
				return false;
			getList().add("SEND_START: " + getFormat().format(getDate()) + " ["+ comment+"]");
			break;
		case SEND_INFO:
			if(f == null)
				return false;
			getList().add("SEND_INFO: " + getFormat().format(getDate()) + " ["+ comment+"]");
			break;
		case SEND_STOP:
			if(f == null)
				return false;
			getList().add("SEND_STOP: " + getFormat().format(getDate()) + " ["+ comment+"]");
			break;
		case SEND_ERROR:
			if(f == null)
				return false;
			getList().add("SEND_ERROR: " + getFormat().format(getDate()) + " ["+ comment+"]");
			break;			
		case STOP:
			if(f == null)
				return false;
			getList().add("STOP: " + getFormat().format(getDate()) + " ["+ comment+"]");
			for(int i =0; i<getList().size();i++)
			{
				update(f, getList().get(i));
			}
/*
            try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(f.getAbsolutePath(), true)))) {
                for(String item : getList())
                    out.println(item);
            }catch (Exception error) {
            }
*/
			f = null;
			break;
		default:
			return false;
		}
		return true;
	}

	@Override
	public String showMyHistory() {
		String myres = "";
		for(int i = 0; i<getList().size(); i++)
			myres +=  getList().get(i) + "\n";
		return myres;
	}
	public ArrayList<String> getList() {
		return list;
	}

	public SimpleDateFormat getFormat() {
		return format;
	}	
	
	public Date getDate() {
		return new Date();
	}	
	
	public File getF() {
		return f;
	}

	public void setF(File f) {
		this.f = f;
	}
	
	public void write(File f, String txt) throws IOException {
		try {

		        PrintWriter out = new PrintWriter(f.getAbsoluteFile());
		        try {
		            out.print(txt);
		        } finally {
		            out.close();
		        }
		    } catch(IOException e) {
		    	
		        throw new RuntimeException(e);
		    }
		}
	
	public void update(File f, String txt) throws IOException {
	    exists(f);
	    StringBuilder sb = new StringBuilder();
	    String oldFile = read(f);
	    sb.append(oldFile);
	    sb.append(txt);
	    write(f, sb.toString());
	}

	public String read(File f) throws FileNotFoundException {
	    StringBuilder sb = new StringBuilder(); 
	    exists(f);
	    try {

	        BufferedReader in = new BufferedReader(new FileReader( f.getAbsoluteFile()));
	        try {
	            String s;
	            while ((s = in.readLine()) != null) {
	                sb.append(s);
	                sb.append("\n");
	            }
	        } finally {

	            in.close();
	        }
	    } catch(IOException e) {
	        throw new RuntimeException(e);
	    }

	    return sb.toString();
	}
	private void exists(File f) throws FileNotFoundException {
		f = new File("log.txt");
	    if (!f.exists()){
	        throw new FileNotFoundException(f.getName());
	    }
		
	}

}
