package org.iu761.vns.logger;

import java.io.*;


public interface  AbstractLogger {
	boolean log(LogMessage mess, String comment) throws Exception;
	String showMyHistory() throws FileNotFoundException;
}
