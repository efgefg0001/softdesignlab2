package org.iu761.vns.logger;

import java.io.FileNotFoundException;
import java.io.IOException;

public class Logger {	
	
	private AbstractLogger my_log_f;
	private static volatile Logger instance;
	
	private Logger(){};
	private Logger(AbstractLogger log_type){
		this.my_log_f = log_type;
	};
	public static synchronized Logger getInstance(Type mtp)
	{
		if(instance == null)
		{
			switch (mtp)
			{
			case SERVER:
				instance = new Logger(new LoggerForServer());
			break;
			case CLIENT:
				instance = new Logger(new LoggerForClient());
			break;
			default:
				instance = new Logger(new NoneLogger());
			break;	
			}
		}
		return instance;
			
	}
	
	public boolean log(LogMessage mess, String comment) throws Exception
	{
		return my_log_f.log(mess,comment);
	}
	
	public String ShowHistory() throws FileNotFoundException
	{
		return my_log_f.showMyHistory();
	}
	
}
