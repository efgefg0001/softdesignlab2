package org.iu761.vns.logger;

public class NoneLogger implements AbstractLogger {
	@Override
	public boolean log(LogMessage mess, String comment) {
		return false;
	}

	@Override
	public String showMyHistory() {
		return "Warning! Logger has NONE type!";
	}
	

}
